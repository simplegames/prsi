﻿using Prsi.Model;
using Prsi.Model.Cards;
using Prsi.Model.Graphics;
using Prsi.Model.Logic;
using Prsi.Model.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prsi
{
    public partial class GameField : Form
    {
        public GameMenu GameMenu { get; }
        public GameField()
        {
            InitializeComponent();
            GameMenu = new GameMenu(this);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GameMenu.Render();
        }
    }
}
