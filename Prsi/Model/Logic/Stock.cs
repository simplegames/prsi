﻿using Prsi.Model.Cards;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Prsi.Model.Logic
{

    class Stock
    {
        public List<ICard> Cards { get; private set; }

        public Stock(List<ICard> deck)
        {
            Cards = deck;
        }

        public List<ICard> GetCards(int count)
        {
            try
            {
                List<ICard> cardsToGet = Cards.Take(count).ToList();
                Cards.RemoveRange(0, count);
                return cardsToGet;
            }
            catch (ArgumentException)
            {
                throw new ArgumentException($"EXCEPTION: More cards taken from stock ({count}) than it has ({Cards.Count})");
            }
        }

        public void PutCards(List<ICard> cards)
        {
            Cards.AddRange(cards);
        }

        public void Shuffle()
        {
            Cards = Cards.OrderBy(x => Guid.NewGuid()).ToList();
        }
    }
}
