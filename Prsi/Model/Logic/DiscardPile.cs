﻿using Prsi.Model.Cards;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Prsi.Model.Logic
{
    class DiscardPile
    {
        public List<ICard> Cards { get; private set; }
        public DiscardPile()
        {
             Cards = new List<ICard>();
        }

        public void PutCard(ICard card)
        {
            Cards.Insert(0, card);
        }

        public List<ICard> GetCards(int count)
        {
            try
            {
                List<ICard> cardsToGet = Cards.Take(count).ToList();
                Cards.RemoveRange(0, count);
                return cardsToGet;
            }
            catch (ArgumentException)
            {
                throw new ArgumentOutOfRangeException($"More cards taken from discard pile ({count}) than it has ({Cards.Count})");
            }
        }
    }
}
