﻿using Prsi.Model.CardEffects;
using Prsi.Model.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Prsi.Model.Logic
{
    class GameLogic
    {
        public Player PlayerAtTurn { get; private set; }
        public Player PlayerNotAtTurn { get; private set; }
        public Stock Stock { get; }
        public DiscardPile DiscardPile { get; }
        public ICard ActiveCard { get; set; }
        public IEffect ActiveEffect { get; set; }
        public GameLogic()
        {
            PlayerAtTurn = new Player("Hráč 1");
            PlayerNotAtTurn = new Player("Hráč 2");
            Stock = new Stock(Config.AllCards);
            DiscardPile = new DiscardPile();
        }
        public void Startup()
        {
            Stock.Cards.OfType<ISpecialCard>().ToList().ForEach(card => card.Effect = (IEffect)Activator.CreateInstance(card.EffectType, this));
            Stock.Shuffle();
            PlayerAtTurn.DrawCards(Stock.GetCards(Config.StartingHandSize));
            PlayerNotAtTurn.DrawCards(Stock.GetCards(Config.StartingHandSize));
            ActiveCard = Stock.GetCards(1)[0];
            DiscardPile.PutCard(ActiveCard);
        }
        public void PrepareTurn(List<Panel> playerCardControls, List<Panel> opponentCardControls)
        {
            PlayerAtTurn.SetActiveCards(playerCardControls);
            PlayerNotAtTurn.SetActiveCards(opponentCardControls);
        }
        public void NextTurn()
        {
            Player tmp = PlayerAtTurn;
            PlayerAtTurn = PlayerNotAtTurn;
            PlayerNotAtTurn = tmp;
        }
        public bool IsCardPlayable(ICard card)
        {
            if (ActiveEffect == null)
            {
                return card.Value == ActiveCard.Value || card.Suit == ActiveCard.Suit;
            } else
            {
                return ActiveEffect.IsCardPlayable(card);
            }
        }
    }
}
