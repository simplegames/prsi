﻿using Prsi.Model.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Prsi.Model.Logic
{
    class Player
    {
        public string Name { get; }
        public List<ICard> Hand { get; private set; }
        public Dictionary<Panel, ICard> ActiveCards { get; private set; }

        public Player(string name)
        {
            Name = name;
            Hand = new List<ICard>();
            ActiveCards = new Dictionary<Panel, ICard>();
        }

        public void DrawCards(List<ICard> cards)
        {
            Hand.AddRange(cards);
        }

        public List<ICard> GetCards(int count)
        {
            List<ICard> handCopy = Hand;
            List<ICard> cardsToTake = handCopy.OrderBy(x => Guid.NewGuid()).Take(count).ToList();
            cardsToTake.ForEach(card => Hand.RemoveAt(Hand.IndexOf(card)));
            return cardsToTake;
        }

        public ICard PlayCard(ICard card)
        {
            card.Play();
            Hand.Remove(card);
            return card;
        }

        public void SetActiveCards(List<Panel> cardControls)
        {
            ActiveCards = cardControls.Zip(Hand, (k, v) => new { Key = k, Value = v })
                     .ToDictionary(x => x.Key, x => x.Value);
        }

    }
}
