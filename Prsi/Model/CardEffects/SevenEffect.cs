﻿using Prsi.Model.Cards;
using Prsi.Model.Logic;
using System;

namespace Prsi.Model.CardEffects
{
    class SevenEffect : IEffect, IDrawForcingEffect
    {
        public GameLogic GameLogic { get; }
        public int NumberOfCardsToDraw { get; set; } = Config.CardsDrawnBySeven;
        public SevenEffect(GameLogic gameLogic)
        {
            GameLogic = gameLogic;
        }
        public void Trigger()
        {
            if (GameLogic.ActiveEffect == null)
            {
                GameLogic.ActiveEffect = this;
            } else if (GameLogic.ActiveEffect.GetType() == GetType())
            {
                ((SevenEffect)GameLogic.ActiveEffect).NumberOfCardsToDraw += Config.CardsDrawnBySeven;
            }
        }
        public bool IsCardPlayable(ICard card)
        {
            return card.Value == GameLogic.ActiveCard.Value;
        }
    }
}
