﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prsi.Model.CardEffects
{
    interface IDrawForcingEffect
    {
        int NumberOfCardsToDraw { get; set; }
    }
}
