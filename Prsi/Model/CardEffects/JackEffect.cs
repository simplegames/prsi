﻿using Prsi.Model.Cards;
using Prsi.Model.Logic;

namespace Prsi.Model.CardEffects
{
    class JackEffect : IEffect
    {
        public GameLogic GameLogic { get; }
        public JackEffect(GameLogic gameLogic)
        {
            GameLogic = gameLogic;
        }
        public void Trigger()
        {

        }
        public bool IsCardPlayable(ICard card)
        {
            return true;
        }
    }
}
