﻿using Prsi.Model.Cards;
using Prsi.Model.Logic;

namespace Prsi.Model.CardEffects
{
    class AceEffect : IEffect, IDrawForbiddingEffect
    {
        public GameLogic GameLogic { get; }
        public AceEffect(GameLogic gameLogic)
        {
            GameLogic = gameLogic;
        }
        public void Trigger()
        {
            if (GameLogic.ActiveEffect == null)
            {
                GameLogic.ActiveEffect = this;
            }
        }
        public bool IsCardPlayable(ICard card)
        {
            return card.Value == GameLogic.ActiveCard.Value;
        }
    }
}
