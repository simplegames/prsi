﻿using Prsi.Model.Cards;
using Prsi.Model.Logic;

namespace Prsi.Model.CardEffects
{
    interface IEffect
    {
        GameLogic GameLogic { get; }
        void Trigger();
        bool IsCardPlayable(ICard card);
    }
}
