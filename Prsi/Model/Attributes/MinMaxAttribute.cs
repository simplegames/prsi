﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prsi.Model.Attributes
{
    class MinMaxAttribute : Attribute
    {
        public string DisplayName { get; private set; }
        public int Min { get; private set; }
        public int Max { get; private set; }
        public MinMaxAttribute(int min, int max)
        {
            Min = min;
            Max = max;
        }
    }
}
