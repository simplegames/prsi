﻿using Prsi.Model.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prsi.Model.Menu
{
    public class GameMenu
    {
        private GameField GameField { get; }
        private UtilGraphics Utils { get; }
        private Settings Settings { get; }
        private const string Name = "Menu";
        public GameMenu(GameField gameField)
        {
            GameField = gameField;
            Settings = new Settings(gameField);
            Utils = new UtilGraphics();
        }
        public void Render()
        {
            Panel overlay = Utils.CreateOverlay(Name);
            overlay.BorderStyle = BorderStyle.FixedSingle;
            FlowLayoutPanel menu = new FlowLayoutPanel
            {
                Anchor = AnchorStyles.None,
                FlowDirection = FlowDirection.TopDown,
                AutoSize = true
            };
            menu.Controls.AddRange(new Control[]
            {
                Utils.CreateHeader("Prší"),
                Utils.CreateButton("Nová hra", NewGameEvent),
                Utils.CreateButton("Nastavení", SettingsEvent),
                Utils.CreateButton("Ukončit", ExitEvent)
            });
            overlay.Controls.Add(menu);
            GameField.Controls.Add(overlay);
            menu.Location = new Point((overlay.Width - menu.Width) / 2, (overlay.Height - menu.Height) / 2);
            overlay.BringToFront();
        }

        public void Clear()
        {
            GameField.Controls.Find(Name, false)[0].Dispose();
        }

        private void ClearAll()
        {
            GameField.Controls.Clear();
        }

        public void ExitEvent(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void NewGameEvent(object sender, EventArgs e)
        {
            Clear();
            Game Game = new Game(GameField);
            Game.Startup();
        }

        public void SettingsEvent(object sender, EventArgs e)
        {
            Clear();
            Settings.Render();
        }

        public void BackToMenuEvent(object sender, EventArgs e)
        {
            ClearAll();
            Render();
        }
    }
}
