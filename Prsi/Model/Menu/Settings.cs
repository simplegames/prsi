﻿using Prsi.Model.Attributes;
using Prsi.Model.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Prsi.Model.Menu
{
    class Settings
    {
        GameField GameField { get; }
        UtilGraphics Utils { get; }

        private static readonly List<PropertyInfo> ConfigProperties = new List<PropertyInfo>
        {
            typeof(Config).GetProperty(nameof(Config.StartingHandSize)),
            typeof(Config).GetProperty(nameof(Config.CardDrawnAmount)),
            typeof(Config).GetProperty(nameof(Config.CardsDrawnBySeven))
        };
        private const string Name = "Settings";
        private const string Plus = "Plus";
        private const string Minus = "Minus";

        public Settings(GameField gameField)
        {
            GameField = gameField;
            Utils = new UtilGraphics();
        }

        public void Render()
        {
            Panel overlay = Utils.CreateOverlay(Name);
            FlowLayoutPanel settings = new FlowLayoutPanel
            {
                Anchor = AnchorStyles.None,
                FlowDirection = FlowDirection.TopDown,
                AutoSize = true
            };
            settings.Controls.Add(Utils.CreateHeader("Nastavení"));
            settings.Controls.AddRange(ConfigProperties.Select(property => CreateRow(property)).ToArray());
            settings.Controls.Add(Utils.CreateButton("Zpět", GameField.GameMenu.BackToMenuEvent));
            overlay.Controls.Add(settings);
            GameField.Controls.Add(overlay);
            settings.Location = new Point((overlay.Width - settings.Width) / 2, (overlay.Height - settings.Height) / 2);
            overlay.BringToFront();
        }

        private TableLayoutPanel CreateRow(PropertyInfo property)
        {
            TableLayoutPanel row = new TableLayoutPanel()
            {
                ColumnCount = 4,
                MinimumSize = new Size(500, 0),
                Height = Config.SquareButtonSize,
                AutoSize = true,
                BackColor = Config.SettingsColor
            };
            row.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
            DisplayNameAttribute propertyAttribute = (DisplayNameAttribute)property.GetCustomAttribute(typeof(DisplayNameAttribute));
            row.Controls.AddRange(new Control[]
            {
                Utils.CreateLabel(propertyAttribute.DisplayName),
                Utils.CreateSquareButton("-", SettingChangeEvent, $"{property.Name}{Minus}"),
                Utils.CreateSquareLabel(property.GetValue(null).ToString()),
                Utils.CreateSquareButton("+", SettingChangeEvent, $"{property.Name}{Plus}")
            });
            return row;
        }

        private void SettingChangeEvent(object sender, EventArgs e)
        {
            Button senderBtn = (Button)sender;
            PropertyInfo propertyToChange = ConfigProperties.Where(property => senderBtn.Name.Contains(property.Name)).ToList()[0];
            MinMaxAttribute minMax = (MinMaxAttribute)propertyToChange.GetCustomAttribute(typeof(MinMaxAttribute));
            int value = (int)propertyToChange.GetValue(null);
            if (senderBtn.Name.Contains(Plus) && value + 1 <= minMax.Max || senderBtn.Name.Contains(Minus) && value - 1 >= minMax.Min)
            {
                propertyToChange.SetValue(null, senderBtn.Name.Contains(Plus) ? value + 1 : value - 1);
                RedrawPropertyValue((Button)sender, propertyToChange);
            }
        }

        private void RedrawPropertyValue(Button changeButton, PropertyInfo propertyInfo)
        {
            changeButton.Parent.Controls[2].Text = propertyInfo.GetValue(null).ToString();
        }

        public void Clear()
        {
            GameField.Controls.Find(Name, false)[0].Dispose();
        }
    }
}
