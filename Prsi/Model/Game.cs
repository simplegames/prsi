﻿using Prsi.Model.CardEffects;
using Prsi.Model.Cards;
using Prsi.Model.Graphics;
using Prsi.Model.Logic;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
namespace Prsi.Model
{
    class Game
    {
        public GameLogic Logic { get; }
        public GameGraphics Graphics { get; }
        private Events Events { get; }

        public Game(GameField game)
        {
            Logic = new GameLogic();
            Graphics = new GameGraphics(game);
            Events = new Events(this);
        }

        public void Startup()
        {
            Logic.Startup();
            PrepareTurn();
        }

        private void PrepareTurn()
        {
            (List<Panel> playerCardControls, List<Panel> opponentCardControls) = Graphics.PrepareTurn(Logic, Events);
            Logic.PrepareTurn(playerCardControls, opponentCardControls);
        }

        public void PlayCard(Panel cardControl)
        {
            ICard playedCard = Logic.PlayerAtTurn.ActiveCards[cardControl];
            if (!Logic.IsCardPlayable(playedCard))
            {
                cardControl.Location = Events.CardLocation;
                MessageBox.Show("Tuto kartu nemůžete zahrát");
                return;
            }
            Logic.ActiveCard = Logic.PlayerAtTurn.PlayCard(playedCard);
            Logic.DiscardPile.PutCard(playedCard);
            if (Logic.PlayerAtTurn.Hand.Count == 0)
            {
                End(Logic.PlayerAtTurn);
            }
            else
            {
                NextTurn();
            }
        }

        public void DrawCard()
        {
            bool turnEnded = true;
            switch (Logic.ActiveEffect)
            {
                case null:
                    Logic.PlayerAtTurn.DrawCards(Logic.Stock.GetCards(Config.CardDrawnAmount));
                    break;
                case IDrawForbiddingEffect _:
                    DialogResult result = MessageBox.Show("Opravdu chcete ukončit kolo bez lízání karty?", $"{Logic.PlayerNotAtTurn.Name} zahrál eso", MessageBoxButtons.YesNo);
                    if (turnEnded = (result == DialogResult.Yes))
                    {
                        Logic.ActiveEffect = null;
                    }
                    break;
                case IDrawForcingEffect effect:
                    if (Logic.Stock.Cards.Count < effect.NumberOfCardsToDraw)
                    {
                        RefreshStock(showMessage: false);
                        MessageBox.Show("Byl otočen odhazovací balíček, nebylo v něm dostatek karet");
                    }
                    if (Logic.Stock.Cards.Count < effect.NumberOfCardsToDraw)
                    {
                        Logic.PlayerAtTurn.DrawCards(Logic.Stock.GetCards(Logic.Stock.Cards.Count));
                        if (effect.NumberOfCardsToDraw >= Logic.PlayerNotAtTurn.Hand.Count)
                        {
                            End(Logic.PlayerNotAtTurn);
                            return;
                        }
                        Logic.PlayerAtTurn.DrawCards(Logic.PlayerNotAtTurn.GetCards(effect.NumberOfCardsToDraw - Logic.Stock.Cards.Count));
                    }
                    else
                    {
                        Logic.PlayerAtTurn.DrawCards(Logic.Stock.GetCards(effect.NumberOfCardsToDraw));
                    }
                    Logic.ActiveEffect = null;
                    break;
            }
            if (turnEnded) NextTurn();
        }

        public void RefreshStock(bool showMessage)
        {
            if (Logic.DiscardPile.Cards.Count == 1)
            {
                if (showMessage) MessageBox.Show("V odhazovacím balíčku je jen 1 karta, nelze ho otočit");
                return;
            }
            Logic.Stock.PutCards(Logic.DiscardPile.GetCards(Logic.DiscardPile.Cards.Count - 1));
            Graphics.Stock.Clear();
            Graphics.Stock.Render(Logic.Stock.Cards.Count == 0, Events.DrawCard, Events.RefreshStock);
        }

        public void NextTurn()
        {
            Logic.NextTurn();
            Graphics.Clear();
            PrepareTurn();
        }

        public void End(Player winner)
        {
            Graphics.Clear();
            Graphics.Interface.RenderOverlay($"{winner.Name} vyhrál", Events.ShowMenu);
        }
    }

}
