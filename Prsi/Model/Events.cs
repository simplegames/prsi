﻿using Prsi.Model.CardEffects;
using Prsi.Model.Cards;
using Prsi.Model.Graphics;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Prsi.Model
{
    class Events
    {
        private Game Game { get; }
        private Point MouseDownLocation { get; set; }
        public Point CardLocation { get; private set; }
        public Events(Game game)
        {
            Game = game;
        }
        public void DrawCard(object sender, EventArgs e) => Game.DrawCard();
        public void MouseDownCard(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            MouseDownLocation = e.Location;
            CardLocation = ((Control)sender).Location;
            ((Control)sender).BringToFront();
        }
        public void MoveCard(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            ((Control)sender).Left = e.X + ((Control)sender).Left - MouseDownLocation.X;
            ((Control)sender).Top = e.Y + ((Control)sender).Top - MouseDownLocation.Y;
        }

        public void ReleaseCard(object sender, MouseEventArgs e)
        {
            Control discardPile = Game.Graphics.GameField.Controls.Find(DiscardPileGraphics.Name, false)[0];
            if (((Control)sender).Bounds.IntersectsWith(discardPile.Bounds))
            {
                Game.PlayCard((Panel)sender);
            } else
            {
                ((Control)sender).Location = CardLocation;
            }
        }

        public void RefreshStock(object sender, EventArgs e) => Game.RefreshStock(showMessage: true);

        public void StartTurn(object sender, EventArgs e)
        {
            Game.Graphics.Interface.ClearOverlay();
        }

        public void ShowMenu(object sender, EventArgs e)
        {
            Game.Graphics.Interface.ClearOverlay();
            Game.Graphics.GameField.GameMenu.Render();
        }
    }
}
