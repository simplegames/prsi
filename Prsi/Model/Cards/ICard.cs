﻿namespace Prsi.Model.Cards
{
    interface ICard
    {
        int Value { get; }

        Suits Suit { get; }

        void Play();
    }
}
