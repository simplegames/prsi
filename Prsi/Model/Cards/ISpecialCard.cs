﻿using Prsi.Model.CardEffects;
using System;

namespace Prsi.Model.Cards
{
    interface ISpecialCard : ICard
    {
        Type EffectType { get; }
        IEffect Effect { get; set; }
    }
}
