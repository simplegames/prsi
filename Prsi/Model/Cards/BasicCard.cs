﻿namespace Prsi.Model.Cards
{
    
    class BasicCard : ICard
    {
        public int Value { get; }
        public Suits Suit { get; }

        public BasicCard(int Value, Suits Suit)
        {
            this.Value = Value;
            this.Suit = Suit;
        }

        public void Play()
        {
            
        }
    }
}
