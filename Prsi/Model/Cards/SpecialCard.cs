﻿using Prsi.Model.CardEffects;
using System;

namespace Prsi.Model.Cards
{
    class SpecialCard : ISpecialCard
    {
        public int Value { get; }
        public Suits Suit { get; }
        public Type EffectType { get; }
        public IEffect Effect { get; set; }
        public SpecialCard(int value, Suits suit, Type effectType)
        {
            Value = value;
            Suit = suit;
            EffectType = effectType;
        }
        public void Play()
        {
            Effect.Trigger();
        }
    }
}