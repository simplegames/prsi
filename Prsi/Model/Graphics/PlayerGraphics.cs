﻿using Prsi.Model.Cards;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Prsi.Model.Graphics
{
    class PlayerGraphics
    {
        private GameField GameField { get; }
        private CardGraphics CardGraphics { get; }
        public PlayerGraphics(GameField gameField)
        {
            GameField = gameField;
            CardGraphics = new CardGraphics();
        }
        public List<Panel> RenderCards(List<ICard> cards, MouseEventHandler cardClickEvent, MouseEventHandler cardMoveEvent, MouseEventHandler cardReleaseEvent)
        {
            List<Panel> cardPanels = new List<Panel>();
            for (int i = 0; i < cards.Count; i++)
            {
                Panel cardControl = CreateCard(cards[i], i);
                //cardControl.Click += playCardEvent;
                cardControl.MouseDown += cardClickEvent;
                cardControl.MouseMove += cardMoveEvent;
                cardControl.MouseUp += cardReleaseEvent;
                foreach (Control control in cardControl.Controls)
                {
                    //control.Click += playCardEvent;
                }
                cardPanels.Add(cardControl);
            }
            return cardPanels;
        }
        private Panel CreateCard(ICard card, int numberOfCardsToLeft)
        {
            Panel cardControl = CardGraphics.CreateFront(card, hasAction: true);
            cardControl.Location = new Point(numberOfCardsToLeft * (Config.CardWidth + Config.GapBetweenCards) + Config.GapBetweenCards, GameField.Height - cardControl.Height);
            GameField.Controls.Add(cardControl);
            return cardControl;
        }
    }
}
