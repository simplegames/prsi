﻿using Prsi.Model.Logic;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Prsi.Model.Graphics
{
    class GameGraphics
    {
        public PlayerGraphics Player { get; }
        public OpponentGraphics Opponent { get; }
        public StockGraphics Stock { get; }
        public DiscardPileGraphics DiscardPile { get; }
        public InterfaceGraphics Interface { get; }
        public GameField GameField { get; }
        public GameGraphics(GameField gameField)
        {
            GameField = gameField;
            Player = new PlayerGraphics(gameField);
            Opponent = new OpponentGraphics(gameField);
            Stock = new StockGraphics(gameField);
            DiscardPile = new DiscardPileGraphics(gameField);
            Interface = new InterfaceGraphics(gameField);
        }
        public void Clear()
        {
            GameField.Controls.Clear();
        }

        public (List<Panel>, List<Panel>) PrepareTurn(GameLogic gameLogic, Events events)
        {
            Interface.RenderOverlay($"{gameLogic.PlayerAtTurn.Name} je na tahu", events.StartTurn);
            DiscardPile.Render(gameLogic.ActiveCard);
            Stock.Render(gameLogic.Stock.Cards.Count == 0, events.DrawCard, events.RefreshStock);
            List<Panel> playerCardControls = Player.RenderCards(gameLogic.PlayerAtTurn.Hand, events.MouseDownCard, events.MoveCard, events.ReleaseCard);
            List<Panel> opponentCardControls = Opponent.RenderCards(gameLogic.PlayerNotAtTurn.Hand);
            return (playerCardControls, opponentCardControls);
        }
    }
}
