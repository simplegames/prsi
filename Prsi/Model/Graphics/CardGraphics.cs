﻿using Prsi.Model.Cards;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace Prsi.Model.Graphics
{
    class CardGraphics
    {
        public Panel CreateFront(ICard card, bool hasAction)
        {
            return new Panel
            {
                Size = new Size(Config.CardWidth, Config.CardHeight),
                BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject($"_{card.Value}{card.Suit.ToString()}"),
                BackgroundImageLayout = ImageLayout.Stretch,
                Cursor = hasAction ? Cursors.Hand : Cursors.Default
            };
        }
        public Panel CreateBack(bool hasAction)
        {
            return new Panel
            {
                Size = new Size(Config.CardWidth, Config.CardHeight),
                BackgroundImage = Properties.Resources.CardBack,
                BackgroundImageLayout = ImageLayout.Stretch,
                Cursor = hasAction ? Cursors.Hand : Cursors.Default
            };
        }
    }
}
