﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Prsi.Model.Graphics
{
    class UtilGraphics
    {
        public Button CreateButton(string text, EventHandler eventHandler, string name = null)
        {
            Button btn = new Button
            {
                Name = name,
                Text = text,
                Font = new Font(Config.FontFamily, 10),
                Size = new Size(Config.ButtonWidth, Config.ButtonHeight),
                Anchor = AnchorStyles.None,
                BackColor = Config.ButtonColor
            };
            btn.Click += eventHandler;
            return btn;
        }

        public Button CreateSquareButton(string text, EventHandler eventHandler, string name = null)
        {
            Button btn = new Button
            {
                Name = name,
                Text = text,
                Font = new Font(Config.FontFamily, 10),
                Size = new Size(Config.SquareButtonSize, Config.SquareButtonSize),
                Anchor = AnchorStyles.None,
                BackColor = Config.ButtonColor,
                Margin = Padding.Empty
            };
            btn.Click += eventHandler;
            return btn;
        }

        public Label CreateLabel(string text) => new Label
        {
            Text = text,
            Font = new Font(Config.FontFamily, 10),
            Anchor = AnchorStyles.Left | AnchorStyles.Right,
            AutoSize = true,
        };

        public Label CreateHeader(string text) => new Label
        {
            Text = text,
            Font = new Font(Config.FontFamily, 24),
            AutoSize = true,
            Anchor = AnchorStyles.None,
            Margin = new Padding(0, 0, 0, 20)
        };

        public Label CreateSquareLabel(string text) => new Label
        {
            Text = text,
            Font = new Font(Config.FontFamily, 10),
            Size = Config.SquareLabelSize,
            TextAlign = ContentAlignment.MiddleCenter,
            BackColor = Config.SquareLabelColor
        };

        public Panel CreateOverlay(string name) => new Panel
        {
            Name = name,
            Dock = DockStyle.Fill,
            BackColor = Config.OverlayColor,
        };
        
    }
}
