﻿using Prsi.Model.Cards;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Prsi.Model.Graphics
{
    class OpponentGraphics
    {
        private GameField GameField { get; }
        private CardGraphics CardGraphics { get; }
        public OpponentGraphics(GameField gameField)
        {
            GameField = gameField;
            CardGraphics = new CardGraphics();
        }
        public List<Panel> RenderCards(List<ICard> cards)
        {
            List<Panel> cardPanels = new List<Panel>();
            for (int i = 0; i < cards.Count; i++)
            {
                cardPanels.Add(CreateCard(i));
            }
            return cardPanels;
        }
        private Panel CreateCard(int numberOfCardsToLeft)
        {
            Panel cardControl = CardGraphics.CreateBack(hasAction: false);
            cardControl.Location = new Point(numberOfCardsToLeft * (Config.CardWidth + Config.GapBetweenCards) + Config.GapBetweenCards, 0);
            GameField.Controls.Add(cardControl);
            return cardControl;
        }
    }
}
