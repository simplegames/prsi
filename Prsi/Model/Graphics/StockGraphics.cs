﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Linq;
using System.Diagnostics;

namespace Prsi.Model.Graphics
{
    class StockGraphics
    {
        private GameField GameField { get; }
        private CardGraphics CardGraphics { get; }
        private UtilGraphics Utils { get; }
        private const string Name = "Stock";
        public StockGraphics(GameField gameField)
        {
            GameField = gameField;
            CardGraphics = new CardGraphics();
            Utils = new UtilGraphics();
        }
        public void Render(bool isEmpty, EventHandler drawCardEvent, EventHandler refreshStockEvent)
        {
            if (isEmpty)
            {
                Button btn = Utils.CreateButton("Otočit balíček", refreshStockEvent);
                btn.Name = Name;
                btn.Location = new Point((GameField.Width - btn.Width) / 2 + Config.StockDiscardPileDistance / 2, (GameField.Height - btn.Height) / 2);
                GameField.Controls.Add(btn);
            }
            else
            {
                Panel stock = CardGraphics.CreateBack(hasAction: true);
                stock.Name = Name;
                stock.Location = new Point((GameField.Width - Config.CardWidth) / 2 + Config.StockDiscardPileDistance / 2, (GameField.Height - Config.CardHeight) / 2);
                stock.Click += drawCardEvent;
                GameField.Controls.Add(stock);
            }
        }
        public void Clear() => GameField.Controls.Find(Name, false)[0].Dispose();
    }
}
