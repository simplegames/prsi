﻿using Prsi.Model.Cards;
using System.Drawing;
using System.Windows.Forms;

namespace Prsi.Model.Graphics
{
    class DiscardPileGraphics
    {
        private GameField GameField { get; }
        private CardGraphics CardGraphics { get; }
        public const string Name = "DiscardPile";
        public DiscardPileGraphics(GameField gameField)
        {
            GameField = gameField;
            CardGraphics = new CardGraphics();
        }
        public void Render(ICard card)
        {
            Panel activeCard = CardGraphics.CreateFront(card, hasAction: false);
            activeCard.Name = Name;
            activeCard.Location = new Point((GameField.Width - Config.CardWidth) / 2 - Config.StockDiscardPileDistance / 2, (GameField.Height - Config.CardHeight) / 2);
            GameField.Controls.Add(activeCard);
        }
    }
}
