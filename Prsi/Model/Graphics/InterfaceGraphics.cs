﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Prsi.Model.Graphics
{
    class InterfaceGraphics
    {
        private GameField GameField { get; }
        private UtilGraphics Utils { get; }
        private const string OverlayName = "Overlay";
        public InterfaceGraphics(GameField gameField)
        {
            GameField = gameField;
            Utils = new UtilGraphics();
        }
        public void RenderOverlay(string message, EventHandler btnEventHandler = null)
        {
            Panel overlay = Utils.CreateOverlay(OverlayName);
            overlay.Controls.Add(Utils.CreateButton(message, btnEventHandler));
            GameField.Controls.Add(overlay);
            overlay.BringToFront();
        }

        public void ClearOverlay()
        {
            GameField.Controls.Find(OverlayName, false)[0].Dispose();
        }
    }
}
