﻿using Prsi.Model.Attributes;
using Prsi.Model.CardEffects;
using Prsi.Model.Cards;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Prsi.Model
{
    static class Config
    {
        public static readonly Rectangle ScreenSize = Screen.PrimaryScreen.Bounds;
        public static readonly double ScreenResolution = Screen.PrimaryScreen.Bounds.Width / (double)Screen.PrimaryScreen.Bounds.Height;
        public static readonly FontFamily FontFamily = FontFamily.GenericSansSerif;
        public const int StockDiscardPileDistance = 400;
        public const int CardWidth = 283;
        public const int CardHeight = 500;
        public const int GapBetweenCards = 25;
        public const int CardLabelSize = 60;
        public const int CardLabelTextSize = 16;
        public const int ButtonWidth = 200;
        public const int ButtonHeight = 50;
        public const int SquareButtonSize = 50;
        public static readonly Size SquareLabelSize = new Size(50, 50);
        public static readonly Color SquareLabelColor = Color.Bisque;
        public static readonly Color ButtonColor = Color.SandyBrown;
        public static readonly Color OverlayColor = Color.Cornsilk;
        public static readonly Color SettingsColor = Color.Bisque;

        [Attributes.DisplayName("Počet karet na začátku")]
        [MinMax(0, 10)]
        public static int StartingHandSize { get; set; } = 7;
        [Attributes.DisplayName("Počet lízaných karet")]
        [MinMax(0, 5)]
        public static int CardDrawnAmount { get; set; } = 2;
        [Attributes.DisplayName("Počet karet na 1 sedmičku")]
        [MinMax(0, 5)]
        public static int CardsDrawnBySeven { get; set; } = 4;

        public static readonly List<ICard> AllCards = new List<ICard>()
        {
            new SpecialCard(7, Suits.Clubs, typeof(SevenEffect)),
            new BasicCard(8, Suits.Clubs),
            new BasicCard(9, Suits.Clubs),
            new BasicCard(10, Suits.Clubs),
            new BasicCard(11, Suits.Clubs),
            new SpecialCard(12, Suits.Clubs, typeof(JackEffect)),
            new BasicCard(13, Suits.Clubs),
            new SpecialCard(14, Suits.Clubs, typeof(AceEffect)),

            new SpecialCard(7, Suits.Diamonds, typeof(SevenEffect)),
            new BasicCard(8, Suits.Diamonds),
            new BasicCard(9, Suits.Diamonds),
            new BasicCard(10, Suits.Diamonds),
            new BasicCard(11, Suits.Diamonds),
            new SpecialCard(12, Suits.Diamonds, typeof(JackEffect)),
            new BasicCard(13, Suits.Diamonds),
            new SpecialCard(14, Suits.Diamonds, typeof(AceEffect)),

            new SpecialCard(7, Suits.Hearts, typeof(SevenEffect)),
            new BasicCard(8, Suits.Hearts),
            new BasicCard(9, Suits.Hearts),
            new BasicCard(10, Suits.Hearts),
            new BasicCard(11, Suits.Hearts),
            new SpecialCard(12, Suits.Hearts, typeof(JackEffect)),
            new BasicCard(13, Suits.Hearts),
            new SpecialCard(14, Suits.Hearts, typeof(AceEffect)),

            new SpecialCard(7, Suits.Spades, typeof(SevenEffect)),
            new BasicCard(8, Suits.Spades),
            new BasicCard(9, Suits.Spades),
            new BasicCard(10, Suits.Spades),
            new BasicCard(11, Suits.Spades),
            new SpecialCard(12, Suits.Spades, typeof(JackEffect)),
            new BasicCard(13, Suits.Spades),
            new SpecialCard(14, Suits.Spades, typeof(AceEffect)),
        };
    }
}
