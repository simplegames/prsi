# Mau-Mau (Prší)
A traditional Europian card game Mau-Mau made in C# Windows Forms for a school project.

## Features
- Use of LINQ
- C# Attributes
- Adjustable settings
- Clean OOP design

## Used technologies
- Windows Forms
